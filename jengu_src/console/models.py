from django.db import models

from django.contrib.auth.models import User
from django.core.validators import MinValueValidator

from datetime import timedelta
from .validators import is_base64
from django.db.models import Q

class Client(models.Model):
    class Meta:
        indexes  = [
            models.Index(fields=['status'])
        ]
        constraints = [
            models.CheckConstraint(check= ~Q(is_legal=False) | Q(legal_representative__isnull=False),
                name="if_not_legal_has_representative")
        ]
    fk_owner = models.ForeignKey(User, on_delete=models.CASCADE)
    first_name = models.TextField(validators = [is_base64], help_text="First Name")
    last_name = models.TextField( validators = [is_base64], help_text="Last Name")
    birth_date = models.DateField(null=True, help_text="Date of Birth")
    inscription = models.DateTimeField(auto_now_add=True, editable=False)
    phone = models.TextField(null=True, validators = [is_base64], help_text="Phone number")
    mail = models.TextField(null=True, validators = [is_base64], help_text="Email address")
    notes = models.TextField(null=True, validators = [is_base64], help_text="Notes")
    status = models.CharField(max_length=6, default = "active", choices=[('ACTIVE','active'),('FORMER','former'), ('BANNED','banned')], help_text="Is he (she) currently an active client")
    is_legal = models.BooleanField(default=True, help_text="Is he (she) of lawful age and legally competent? If not, Indicate a legal representative")
    legal_representative = models.TextField(null=True, validators = [is_base64], help_text="Legal representative (if not legally competent)")

    def __str__(self): 
        return "{}: {} {}".format(self.id, self.first_name, self.last_name)        

class PrestationType(models.Model):
    denomination = models.CharField(max_length=80, unique=True)
    typical_duration = models.DurationField()
    typical_price = models.FloatField(validators=[MinValueValidator(0.0)])

    def __str__(self): 
        return self.denomination

class Consultation(models.Model):
    class Meta:
        indexes  = [
            models.Index(fields=['status'])
        ]
    fk_owner = models.ForeignKey(User, on_delete=models.CASCADE)
    fk_client = models.ManyToManyField(Client)
    fk_type = models.ForeignKey(PrestationType, null=True, on_delete=models.SET_NULL, help_text="Prestation type")
    date = models.DateTimeField()
    duration = models.DurationField()
    payed = models.FloatField(null=True, validators=[MinValueValidator(0.0)])
    status = models.CharField(max_length=9, choices=[('booked','booked'),('cancelled','cancelled'), ('done','done')])
